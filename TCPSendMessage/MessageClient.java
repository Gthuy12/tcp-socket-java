package TCPSendMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.Socket;

public class MessageClient extends JFrame implements ActionListener {

	public JButton send;
	public JTextField message;
	public JTextArea content;
	
	String temp = "";
	Socket socket;
	ObjectOutputStream oos;
    ObjectInputStream ois;
    
    public void GUI() {

        setSize(600, 450);
        getContentPane().setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        Font fo = new Font("Arial", Font.PLAIN, 15);
        content = new JTextArea();
        content.setFont(fo);
        content.setBackground(Color.white);

        content.setEditable(false);
        JScrollPane sp = new JScrollPane(content);
        sp.setBounds(50, 50, 500, 200);
        send = new JButton("Send");
        send.addActionListener(this);
        send.setBounds(480, 290, 70, 40);
        message = new JTextField("");
        message.setFont(fo);
        message.setBounds(50, 300, 400, 30);
        message.setBackground(Color.white);
        add(message);
        add(send);
        add(sp);
        setVisible(true);
    }
    
    public MessageClient(String str) throws IOException, ClassNotFoundException {
        super(str);
        GUI();
        socket = new Socket(str,7070);
        System.out.println("Da ket noi toi server!");
        oos = new ObjectOutputStream(socket.getOutputStream());
        ois = new ObjectInputStream(socket.getInputStream());
        while(true) {
            String stream = ois.readObject().toString();
            temp += stream + '\n';
            content.setText(temp);
        }
    }
    
    public static void main(String[] args) throws Exception {
        MessageClient obj = new MessageClient("localhost");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == send) {
            try {
                temp += message.getText() + "\n";
                content.setText(temp);
                oos.writeObject(message.getText());
                message.setText("");
                message.requestFocus();
                content.setVisible(false);
                content.setVisible(true);
            } catch (Exception r) {
                r.printStackTrace();
            }
        }

    }
}
