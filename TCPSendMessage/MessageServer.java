package TCPSendMessage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class MessageServer {
	public static void main(String[] args) throws Exception {
        ServerSocket server = new ServerSocket(7070);
        while (true) {
            Socket socket = null;
            try {
                System.out.println("Dang doi client...");
                socket = server.accept();
                System.out.println("Client da ket noi!");
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Thread t = new HandlerString(socket, ois, oos);
                t.start();
            } catch (Exception e) {
                socket.close();
                e.printStackTrace();
            }
        }
    }
}

class HandlerString extends Thread {
    public String ConvertToUpperCase(String r) {
    	String s = "";
		for(int i = 0; i < r.length(); i++) {
			if( (int)r.charAt(i) < 143 && (int)r.charAt(i) > 96 ) {
				s+= (char)((int)r.charAt(i) - 32);
			} else {
				s+= r.charAt(i);
			}
		}
		return s;
    }
    public String ConvertToLowerCase(String r) {
    	String s = "";
		for(int i = 0; i < r.length(); i++) {
			if((int)r.charAt(i) < 91 && (int)r.charAt(i) > 64) {
				s+= (char)((int)r.charAt(i) + 32);
			} else {
				s+= r.charAt(i);
			}
		}
		return s;
    }
    public String UpcaseOrLowercase(String r) {
    	String s = "";
		for(int i = 0; i < r.length(); i++) {
			if((int)r.charAt(i) < 91 && (int)r.charAt(i) > 64) {
				s+= (char)((int)r.charAt(i) + 32);
			} else if( (int)r.charAt(i) < 143 && (int)r.charAt(i) > 96 ) {
				s+= (char)((int)r.charAt(i) - 32);
			} 
		}
		return s;
    }
    public int CountWord(String str){
        int count = 0;
        char ch[] = new char[str.length()];
        if(str.isEmpty() || str == null){
            return 0;
        }
        if(str.length() > 0){
            for(int i = 0; i < str.length(); i++){
                ch[i] = str.charAt(i);
                if( ((i>0)&&(ch[i]!=' ')&&(ch[i-1]==' ')) || ((ch[0]!=' ')&&(i==0)) ){
                    count++;
                }
            }
        }
        return count;
    }

    public int CountVowel(String str){
        int count = 0;
        char ch[] = new char[str.length()];
        if(str.isEmpty() || str == null){
            return 0;
        }
        if(str.length() > 0){
            for(int i = 0; i < str.length(); i++)
            {
                ch[i] = str.charAt(i);
                if(ch[i] == 65||ch[i] == 69||ch[i]==73||ch[i]==79||ch[i]==85||ch[i]==97||ch[i]==101||ch[i]==105||ch[i]==111||ch[i]==117)
                {
                    count++;
                }
            }
        }
        return count;
    }

    final ObjectInputStream ois;
    final ObjectOutputStream oos;
    final Socket s;


    // Constructor
    public HandlerString(Socket s, ObjectInputStream ois, ObjectOutputStream oos)
    {
        this.s = s;
        this.ois = ois;
        this.oos = oos;
    }

    @Override
    public void run()
    {
        while (true)
        {
            try {
                String str = ois.readObject().toString();
                System.out.println(str);
                oos.writeObject("In hoa: " + ConvertToUpperCase(str) + "\n"
                              + "In thuong: " + ConvertToLowerCase(str) + "\n"
                              + "In hoa thuong: " + UpcaseOrLowercase(str) + "\n"
                              + "So tu: " + CountWord(str) + "\n"
                              + "So nguyen am: " + CountVowel(str) + "\n");

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
