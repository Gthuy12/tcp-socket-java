package TCPCalculate;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class CalculateServer {
	public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(7070);
        while (true) {
            Socket socket = null;
            try {
                socket = server.accept();
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Thread t = new HandleString(socket, oos, ois);
                t.start();
            } catch (Exception e) {
                socket.close();
                e.printStackTrace();
            }
        }
    }
}

class HandleString extends Thread
{
    final ObjectOutputStream oos;
    final ObjectInputStream ois;
    final Socket s;

    // Constructor
    public HandleString(Socket s, ObjectOutputStream oos, ObjectInputStream ois)
    {
        this.s = s;
        this.oos = oos;
        this.ois = ois;
    }

    @Override
    public void run()
    {
        while (true)
        {
            try {
                String stream = ois.readObject().toString();
                double result = InfixToPostfix.calculate(stream);
                System.out.println(result);
                oos.writeObject("Ket qua cua phep tinh " + stream + " = " + result);


            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}