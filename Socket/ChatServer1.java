package Socket;
import java.io.*;
import java.util.*;
import java.net.*;

public class ChatServer1 {
	public void main(String[] args) throws Exception {
		ServerSocket server = new ServerSocket(7070);
		System.out.print("Server is started");
		Socket socket = server.accept();
		DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
		DataInputStream din = new DataInputStream(socket.getInputStream());
		Scanner kb = new Scanner(System.in);
		while(true) {
			String st = din.readUTF();
			System.out.println(st);
			System.out.print("Server: ");
			String msg = kb.nextLine();
			String Upper = ConvertToUpperCase(msg);
			dos.writeUTF("Server: " + Upper);
			dos.flush();
			kb = kb.reset();
		}
	}
	public String ConvertToUpperCase(String r) {
		String s = "";
		for(int i = 0; i < r.length(); i++) {
			if( (int)r.charAt(i) < 143 && (int)r.charAt(i) > 96 ) {
				s+= (char)((int)r.charAt(i) - 32);
			} else {
				s+= r.charAt(i);
			}
		}
		return s;
	}
	public String ConvertToLowerCase(String r) {
		String s = "";
		for(int i = 0; i < r.length(); i++) {
			if((int)r.charAt(i) < 91 && (int)r.charAt(i) > 64) {
				s+= (char)((int)r.charAt(i) + 32);
			} else {
				s+= r.charAt(i);
			}
		}
		return s;
	}
	public String UpcaseOrLowercase(String r) {
		String s = "";
		for(int i = 0; i < r.length(); i++) {
			if((int)r.charAt(i) < 91 && (int)r.charAt(i) > 64) {
				s+= (char)((int)r.charAt(i) + 32);
			} else if( (int)r.charAt(i) < 143 && (int)r.charAt(i) > 96 ) {
				s+= (char)((int)r.charAt(i) - 32);
			} 
		}
		return s;
	}
}
